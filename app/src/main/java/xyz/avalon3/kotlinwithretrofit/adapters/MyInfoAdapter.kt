package xyz.avalon3.kotlinwithretrofit.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import kotlinx.android.synthetic.main.single_info_layout.view.*
import xyz.avalon3.kotlinwithretrofit.R

/**
 * Created by Golam Kibria on 10/6/2018.
 */
class MyInfoAdapter(private val itemArrayList: ArrayList<String>) : RecyclerView.Adapter<MyInfoAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.single_info_layout, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.textView.text = itemArrayList[position]
    }

    override fun getItemCount(): Int {
        return itemArrayList.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textView: TextView = itemView.tv_info
    }
}