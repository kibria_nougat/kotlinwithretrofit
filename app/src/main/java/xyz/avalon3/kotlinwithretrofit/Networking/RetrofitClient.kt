package xyz.avalon3.kotlinwithretrofit.Networking

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import xyz.avalon3.kotlinwithretrofit.MyInterface

/**
 * Created by Golam Kibria on 10/6/2018.
 */
class RetrofitClient{

    val service: MyInterface

    companion object {
        const val BASE_URL = "https://api.github.com/"
    }

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL) //1
                .addConverterFactory(GsonConverterFactory.create()) //2
                .build()
        service = retrofit.create(MyInterface::class.java) //3
    }

}