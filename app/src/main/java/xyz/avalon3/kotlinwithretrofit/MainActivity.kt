package xyz.avalon3.kotlinwithretrofit

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log.d
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Response
import xyz.avalon3.kotlinwithretrofit.Networking.RetrofitClient
import xyz.avalon3.kotlinwithretrofit.models.RepoResult

class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var mLinearLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = my_recycler_view
        mLinearLayoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = mLinearLayoutManager

        getData()
    }

    private fun getData(){

        val retrofitClient = RetrofitClient()
        retrofitClient.service.searchRepositories().enqueue(object : retrofit2.Callback<RepoResult>{
            override fun onFailure(call: Call<RepoResult>?, t: Throwable?) {
                d("DataTesting", "onFailure")
            }

            override fun onResponse(call: Call<RepoResult>?, response: Response<RepoResult>?) {
                val resultList = RepoResult(response?.body()?.items ?: emptyList())
                d("DataTesting", resultList.toString())
            }

        })
    }

}
