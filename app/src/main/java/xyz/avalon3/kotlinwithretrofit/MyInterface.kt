package xyz.avalon3.kotlinwithretrofit

import retrofit2.Call
import retrofit2.http.GET
import xyz.avalon3.kotlinwithretrofit.models.RepoResult

/**
 * Created by Golam Kibria on 10/6/2018.
 */
interface MyInterface {

    @GET("/repositories")
    fun retrieveRepositories(): Call<RepoResult>

    @GET("/search/repositories?q=language:kotlin&sort=stars&order=desc") //sample search
    fun searchRepositories(): Call<RepoResult>
}